CREATE TABLE `tx_formsave_domain_model_document` (
  `uid`       int(11)              NOT NULL AUTO_INCREMENT,
  `pid`       int(11)              NOT NULL DEFAULT '0',
  `source_id` int(11)              NOT NULL DEFAULT '0',
  `deleted`   tinyint(1)           NOT NULL DEFAULT '0',
  `tstamp`    int(11)              NOT NULL DEFAULT '0',
  `crdate`    int(11)              NOT NULL DEFAULT '0',
  `cruser_id` int(11)              NOT NULL DEFAULT '0',
  `origUid`   int(11)              NOT NULL DEFAULT '0',
  `ip`        varchar(45)          NOT NULL DEFAULT '',
  `entries`   smallint(5) unsigned NOT NULL DEFAULT '0',
  `type`      varchar(200)         NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `pid` (`pid`)
);

CREATE TABLE `tx_formsave_domain_model_entry` (
  `uid`             int(11)      NOT NULL AUTO_INCREMENT,
  `parent_uid`      int(11)      NOT NULL DEFAULT '0',
  `pid`             int(11)      NOT NULL DEFAULT '0',
  `deleted`         tinyint(1)   NOT NULL DEFAULT '0',
  `tstamp`          int(11)      NOT NULL DEFAULT '0',
  `crdate`          int(11)      NOT NULL DEFAULT '0',
  `cruser_id`       int(11)      NOT NULL DEFAULT '0',
  `origUid`         int(11)      NOT NULL DEFAULT '0',
  `identifier`      varchar(50)  NOT NULL DEFAULT '',
  `form_identifier` varchar(50)  NOT NULL DEFAULT '',
  `form_label`      varchar(200) NOT NULL DEFAULT '',
  `form_properties` text                  DEFAULT NULL,
  `form_value`      mediumtext            DEFAULT NULL,
  `form_type`       varchar(50)  NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `pid` (`pid`),
  KEY `parent_uid` (`parent_uid`),
  KEY `identifier` (`identifier`, `parent_uid`),
  UNIQUE KEY `form_identifier` (`form_identifier`, `parent_uid`),
  KEY `form_label` (`form_label`, `parent_uid`)
);
