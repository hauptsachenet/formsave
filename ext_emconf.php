<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'formsave',
    'description' => '',
    'version' => '1.6.3',
    'category' => '',
    'author' => 'Marco Pfeiffer',
    'author_email' => 'marco@hauptsache.net',
    'author_company' => 'hauptsache.net',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'constraints' => array(
        'depends' => array(
            'typo3' => '8.7.7-8.7.99',
            'form' => '8.7.7-8.7.99'
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);
