<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['EXT']['formsave']['writer'] = [
    'csv' => [
        'label' => 'Text .csv',
        'filename' => 'formsave.csv',
        'mimeType' => 'text/csv',
        'class' => \Hn\Formsave\Exporter\CsvWriter::class,
    ],
    'xlsx' => [
        'label' => 'Excel .xlsx',
        'filename' => 'formsave.xlsx',
        'mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'class' => \Hn\Formsave\Exporter\XlsxWriter::class,
    ],
];
