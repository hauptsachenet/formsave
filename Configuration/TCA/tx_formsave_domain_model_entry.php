<?php

return [
    'ctrl' => [
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'origUid' => 'origUid',
        'tstamp' => 'tstamp',
        'hideTable' => true,

        'title' => 'Form Document',
        'label' => 'form_label',
        'label_alt' => 'form_value',
        'label_alt_force' => true,
    ],
    'columns' => [
        'identifier' => [
            'label' => 'Identifier',
            'config' => [
                'type' => 'input',
                'size' => 25,
                'max' => 50,
                'eval' => 'trim',
            ],
        ],
        'form_identifier' => [
            'label' => 'Form identifier',
            'config' => [
                'type' => 'input',
                'size' => 25,
                'max' => 50,
                'eval' => 'trim',
            ],
        ],
        'form_type' => [
            'label' => 'Form type',
            'config' => [
                'type' => 'input',
                'size' => 25,
                'max' => 50,
                'eval' => 'trim',
            ],
        ],
        'form_label' => [
            'label' => 'Form label',
            'config' => [
                'type' => 'input',
                'size' => 25,
                'max' => 50,
                'eval' => 'trim',
            ],
        ],
        'form_properties' => [
            'label' => 'Form properties',
            'config' => [
                'type' => 'text',
                'eval' => 'trim',
            ]
        ],
        'form_value' => [
            'label' => 'Form value',
            'config' => [
                'type' => 'text',
                'eval' => 'trim',
            ]
        ]
    ],
    'types' => [
        '0' => [
            'showitem' => 'identifier, form_identifier, form_type, form_label, form_properties, form_value'
        ]
    ]
];