<?php

return [
    'ctrl' => [
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'origUid' => 'origUid',
        'tstamp' => 'tstamp',
        'type' => 'type',

        'title' => 'Form Document',
        'label' => 'crdate',
    ],
    'columns' => [
        'type' => [
            'label' => 'Type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['Generic Document', \Hn\Formsave\Domain\Model\Document::class],
                ],
            ],
        ],
        'source_id' => [
            'label' => 'Source id (Page which created this document)',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages',
                'size' => 1,
                'minitems' => 1,
                'maxitems' => 1,
            ],
        ],
        'crdate' => [
            'label' => 'Crdate',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'readOnly' => true,
                'eval' => 'datetime',
            ],
        ],
        'ip' => [
            'label' => 'IP',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'readOnly' => true,
            ],
        ],
        'entries' => [
            'label' => 'Entries',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_formsave_domain_model_entry',
                'foreign_field' => 'parent_uid',
                'foreign_default_sortby' => 'uid',
                'appearance' => [
                    'collapseAll' => true,
                ],
                'behaviour' => [
                    'enableCascadingDelete' => true,
                ],
            ],
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => 'type, crdate, ip, entries',
        ],
    ],
];
