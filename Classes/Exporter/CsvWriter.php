<?php

namespace Hn\Formsave\Exporter;


class CsvWriter extends AbstractTableWriter
{
    /**
     * @var resource|null
     */
    private $handle;

    /**
     * @var string
     */
    private $delimiter;

    /**
     * @var string
     */
    private $enclosure;

    /**
     * @param string $filename
     * @param string $delimiter
     * @param string $enclosure
     *
     * @throws \Exception
     */
    public function __construct(string $filename = 'php://output', string $delimiter = ",", string $enclosure = '"')
    {
        $this->handle = fopen($filename, 'w') ?: null;
        if (!is_resource($this->handle)) {
            throw new \Exception("File $filename couldn't be opened", 1537783581);
        }

        $this->delimiter = $delimiter;
        $this->enclosure = $enclosure;
    }

    public function __destruct()
    {
        if (is_resource($this->handle)) {
            fclose($this->handle);
        }
    }

    protected function doWriteFields(array $fields): void
    {
        fputcsv($this->handle, $fields, $this->delimiter, $this->enclosure);
    }

    protected function doWriteLine(array $mappedValues): void
    {
        fputcsv($this->handle, $mappedValues, $this->delimiter, $this->enclosure);
    }
}
