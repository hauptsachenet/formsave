<?php

namespace Hn\Formsave\Exporter;

interface WriterInterface
{
    /**
     * Sets which fields are to expect in the following writeLine calls.
     *
     * @param string[] $fields
     *
     * @throws \LogicException can be thrown if writeLine was already called
     */
    public function setFields(array $fields): void;

    /**
     * Writes a line of data.
     *
     * @param array $values
     *
     * @throws \LogicException can be thrown setFields aren't called yet
     */
    public function writeLine(array $values): void;

    /**
     * Called at the end.
     *
     * This can be used to finalize the file.
     */
    public function finish(): void;
}
