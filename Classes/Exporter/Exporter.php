<?php

namespace Hn\Formsave\Exporter;

use Hn\Formsave\Domain\Model\Document;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;

class Exporter
{
    const OPTIONS_EXPORT_LABEL = 'label';
    const OPTIONS_CHUNK_SIZE = 'chunk_size';

    public function export(iterable $documents, WriterInterface $writer, array $options = []): void
    {
        $generator = function () use ($documents) {
            foreach ($documents as $document) {
                yield $document;
            }
        };

        $this->exportUsingCallable($generator, $writer, $options);
    }

    /**
     * @param QueryResultInterface $queryResult
     * @param WriterInterface $writer
     * @param array $options
     */
    public function exportUsingQueryResult(QueryResultInterface $queryResult, WriterInterface $writer, array $options = []): void
    {
        $chunkSize = $options[self::OPTIONS_CHUNK_SIZE] ?? 50;
        $count = $queryResult->count();
        $offsets = $count <= $chunkSize ? [0] : range(0, $count - 1, $chunkSize);

        $query = $queryResult->getQuery();
        $generator = function () use ($query, $chunkSize, $offsets) {
            foreach ($offsets as $index => $offset) {
                $query->setOffset($offset);
                $query->setLimit($chunkSize);
                foreach ($query->execute() as $item) {
                    yield $item;
                }

                GeneralUtility::makeInstance(ObjectManager::class)->get(PersistenceManager::class)->clearState();
            }
        };

        $this->exportUsingCallable($generator, $writer, $options);
    }

    /**
     * Fairly memory inefficient but easy. Just pass all documents to export as an array.
     *
     * @param callable $documents
     * @param WriterInterface $writer
     * @param array $options
     */
    public function exportUsingCallable(callable $documents, WriterInterface $writer, array $options = []): void
    {
        $dispatcher = GeneralUtility::makeInstance(Dispatcher::class);

        $fields = [];
        $types = [];

        // gather all possible labels

        /** @var Document $document */
        foreach ($documents() as $document) {
            $types[get_class($document)] = $document;

            foreach ($document->getEntries() as $entry) {
                $identifier = $entry->getIdentifier();
                if ($identifier) {
                    $fields[$identifier] = $identifier;
                    continue;
                }

                if ($options[self::OPTIONS_EXPORT_LABEL] ?? false) {
                    $fields[$entry->getFormLabel()] = $entry->getFormLabel();
                    continue;
                }
            }
        }

        $fields = array_values($fields);
        $dispatcher->dispatch(__CLASS__, 'setFields', [&$fields, array_values($types)]);
        $writer->setFields($fields);

        /** @var Document $document */
        foreach ($documents() as $document) {
            $values = [];

            foreach ($document->getEntries() as $entry) {
                $identifier = $entry->getIdentifier();
                if ($identifier) {
                    $values[$identifier] = $entry->getFormValue();
                    continue;
                }

                if ($options[self::OPTIONS_EXPORT_LABEL] ?? false) {
                    $values[$entry->getFormLabel()] = $entry->getFormValue();
                    continue;
                }
            }

            $dispatcher->dispatch(__CLASS__, 'writeLine', [&$values, $document]);
            $writer->writeLine($values);
        }

        $writer->finish();
    }
}
