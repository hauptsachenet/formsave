<?php

namespace Hn\Formsave\Exporter;


abstract class AbstractTableWriter implements WriterInterface
{
    /**
     * @var array|null
     */
    private $fields = null;

    /**
     * Sets which fields are to expect in the following writeLine calls.
     *
     * @param string[] $fields
     *
     * @throws \LogicException can be thrown if writeLine was already called
     */
    public function setFields(array $fields): void
    {
        if ($this->fields !== null) {
            throw new \LogicException("Fields are already set");
        }

        $fields = array_values($fields);
        $this->fields = array_flip($fields);
        $this->doWriteFields($fields);
    }

    protected function doWriteFields(array $fields): void
    {
        $this->doWriteLine($fields);
    }

    /**
     * Writes a line of data.
     *
     * @param array $values
     *
     * @throws \LogicException can be thrown setFields aren't called yet
     */
    public function writeLine(array $values): void
    {
        if ($this->fields === null) {
            throw new \LogicException("Fields aren't set yet");
        }

        $mappedValues = array_fill(0, max($this->fields), '');
        foreach ($values as $field => $value) {
            if (!isset($this->fields[$field])) {
                throw new \LogicException("Field '$field' was not in the list of fields while using setFields.");
            }

            $mappedValues[$this->fields[$field]] = $value;
        }

        $this->doWriteLine($mappedValues);
    }

    protected abstract function doWriteLine(array $mappedValues): void;

    public function finish(): void
    {
    }
}
