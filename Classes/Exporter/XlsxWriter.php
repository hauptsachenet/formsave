<?php

namespace Hn\Formsave\Exporter;


use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class XlsxWriter extends AbstractTableWriter
{
    private $spreadsheet;

    private $row = 1;

    private $columns = 1;

    private $filename;

    public function __construct(string $filename = 'php://output')
    {
        $this->spreadsheet = new Spreadsheet();
        $this->filename = $filename;
    }

    protected function doWriteLine(array $mappedValues): void
    {
        $worksheet = $this->spreadsheet->getActiveSheet();
        foreach ($mappedValues as $index => $value) {
            $worksheet->setCellValueExplicitByColumnAndRow($index + 1, $this->row, $value, DataType::TYPE_STRING2);
        }
        $this->row++;
        $this->columns = max($this->columns, count($mappedValues));
    }

    public function finish(): void
    {
        $worksheet = $this->spreadsheet->getActiveSheet();
        foreach (range(1, $this->columns) as $columnIndex) {
            $worksheet->getColumnDimensionByColumn($columnIndex)->setAutoSize(true);
        }
        $worksheet->calculateColumnWidths();

        $writer = new Xlsx($this->spreadsheet);
        $writer->save($this->filename);
    }
}
