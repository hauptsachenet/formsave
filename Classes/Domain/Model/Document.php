<?php

namespace Hn\Formsave\Domain\Model;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Document extends AbstractEntity implements \ArrayAccess
{
    /**
     * @var int|null
     */
    protected $sourceId;

    /**
     * @var \DateTime
     */
    protected $crdate;

    /**
     * @var string
     */
    protected $ip;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hn\Formsave\Domain\Model\Entry>
     */
    protected $entries;

    public function __construct(string $ip = null)
    {
        $this->crdate = new \DateTime();
        $this->ip = $ip ?: GeneralUtility::getIndpEnv('REMOTE_ADDR');
        $this->entries = new ObjectStorage();
    }

    public function getSourceId(): ?int
    {
        return $this->sourceId;
    }

    public function setSourceId(?int $sourceId): void
    {
        $this->sourceId = $sourceId;
    }

    public function getCrdate(): \DateTime
    {
        return clone $this->crdate;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @return Entry[]
     */
    public function getEntries(): array
    {
        return $this->entries->toArray();
    }

    public function getEntry($identifier): ?Entry
    {
        foreach ($this->getEntries() as $entry) {
            if ($entry->getIdentifier() === $identifier) {
                return $entry;
            }
        }

        return null;
    }

    public function addEntry(Entry $entry)
    {
        $this->entries->attach($entry);
    }

    public function offsetExists($offset): bool
    {
        foreach ($this->getEntries() as $value) {
            if ($value->getIdentifier() === $offset) {
                return true;
            }
        }

        return false;
    }

    public function offsetGet($offset)
    {
        foreach ($this->getEntries() as $value) {
            if ($value->getIdentifier() === $offset) {
                return $value->getFormValue();
            }
        }

        throw new \OutOfRangeException("Entry $offset does not exist");
    }

    public function offsetSet($offset, $value)
    {
        throw new \LogicException("A document value can't be set");
    }

    public function offsetUnset($offset)
    {
        throw new \LogicException("A document value can't be unset");
    }
}
