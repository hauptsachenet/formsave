<?php

namespace Hn\Formsave\Domain\Model;


use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Entry extends AbstractEntity
{
    /**
     * @var string
     */
    protected $identifier;

    /**
     * @var string
     */
    protected $formIdentifier;

    /**
     * @var string
     */
    protected $formType;

    /**
     * @var string
     */
    protected $formLabel;

    /**
     * @var string
     */
    protected $formProperties;

    /**
     * @var string|null
     */
    protected $formValue;

    public function __construct(string $identifier, string $formIdentifier, string $formType, string $formLabel, array $formProperties, string $formValue = null)
    {
        $this->identifier = $identifier;
        $this->formIdentifier = $formIdentifier;
        $this->formType = $formType;
        $this->formLabel = $formLabel;
        $this->formProperties = json_encode($formProperties, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        $this->formValue = strval($formValue);
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getFormIdentifier(): string
    {
        return $this->formIdentifier;
    }

    public function getFormType(): string
    {
        return $this->formType;
    }

    public function getFormLabel(): string
    {
        return $this->formLabel;
    }

    public function getFormProperties(): array
    {
        return json_decode($this->formProperties, true);
    }

    public function getFormValue(): string
    {
        return $this->formValue;
    }

    public function __toString(): string
    {
        return $this->getFormValue();
    }
}
