<?php

namespace Hn\Formsave\Domain\Finisher;


use Hn\Formsave\Domain\Model\Document;
use Hn\Formsave\Domain\Model\Entry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;
use TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotException;
use TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotReturnException;
use TYPO3\CMS\Extbase\Validation\ValidatorResolver;
use TYPO3\CMS\Form\Domain\Finishers\AbstractFinisher;
use TYPO3\CMS\Form\Domain\Model\FormElements\FormElementInterface;

class FormsaveFinisher extends AbstractFinisher
{
    protected $defaultOptions = [
        'intent' => Document::class,
        'pid' => '',
    ];

    /**
     * @return void
     * @throws InvalidSlotException
     * @throws InvalidSlotReturnException
     * @throws \TYPO3\CMS\Form\Domain\Exception\RenderingException
     */
    protected function executeInternal()
    {
        $dispatcher = GeneralUtility::makeInstance(Dispatcher::class);
        $dispatcher->dispatch(static::class, 'preProcess', [$this->finisherContext]);

        $formValues = $this->finisherContext->getFormValues();
        $formDefinition = $this->finisherContext->getFormRuntime()->getFormDefinition();

        $documentClass = $this->parseOption('intent');
        if (!is_a($documentClass, Document::class, true)) {
            throw new \RuntimeException("Intent must be instance of " . Document::class . " got $documentClass");
        }

        /** @var Document $document */
        $document = GeneralUtility::makeInstance($documentClass);
        $document->setPid($this->parseOption('pid') ?: $GLOBALS['TSFE']->id);
        $document->setSourceId($GLOBALS['TSFE']->id ?: null);

        foreach ($formValues as $formIdentifier => $formValue) {
            $element = $formDefinition->getElementByIdentifier($formIdentifier);
            if (!$element instanceof FormElementInterface || $element->getType() === 'Honeypot') {
                // these here might be honeypot fields
                continue;
            }

            $dispatcher->dispatch(static::class, 'preEntry', [$this->finisherContext, &$formValue, $document, $formIdentifier]);

            if ($formValue instanceof \DateTimeInterface) {
                $formValue = $formValue->format('r');
            }

            if (is_array($formValue)) {
                $formValue = implode(",", $formValue);
            }

            if (!is_string($formValue)) {
                $type = is_object($formValue) ? get_class($formValue) : gettype($formValue);
                $formValue = '[Unhandled type ' . $type . ']';
            }

            $entry = GeneralUtility::makeInstance(
                Entry::class,
                $element->getProperties()['formsaveIdentifier'] ?? '',
                $formIdentifier,
                $element->getType(),
                $element->getLabel(),
                $element->getProperties(),
                $formValue
            );
            $entry->setPid($document->getPid());
            $dispatcher->dispatch(static::class, 'postEntry', [$this->finisherContext, $entry, $document]);
            $document->addEntry($entry);
        }

        $validator = $this->objectManager->get(ValidatorResolver::class)->getBaseValidatorConjunction($documentClass);
        $dispatcher->dispatch(static::class, 'preValidate', [$this->finisherContext, $document, &$validator]);
        $validationResult = $validator->validate($document);
        if ($validationResult->hasErrors()) {
            $this->finisherContext->cancel();
            $this->finisherContext->getFormRuntime()->overrideCurrentPage(0);
            $documentValidationResult = $this->finisherContext->getControllerContext()->getRequest()->getOriginalRequestMappingResults();
            $this->finisherContext->getControllerContext()->getRequest()->setOriginalRequestMappingResults($documentValidationResult);
            foreach ($validationResult->getFlattenedErrors() as $property => $errors) {
                $identifier = $this->finisherContext->getFormRuntime()->getIdentifier();
                if ($document->getEntry($property) !== null) {
                    $identifier .= '.' . $document->getEntry($property)->getFormIdentifier();
                }

                foreach ($errors as $error) {
                    $documentValidationResult->forProperty($identifier)->addError($error);
                }
            }

            return $this->finisherContext->getFormRuntime()->render();
        }

        $dispatcher->dispatch(static::class, 'prePersist', [$this->finisherContext, $document]);
        $persistenceManager = GeneralUtility::makeInstance(ObjectManager::class)->get(PersistenceManager::class);
        $persistenceManager->add($document);
        $persistenceManager->persistAll();
        $dispatcher->dispatch(static::class, 'postPersist', [$this->finisherContext, $document]);
    }
}
