<?php

namespace Hn\Formsave\Domain\Repository;


use Doctrine\DBAL\Connection;
use Hn\Formsave\Domain\Model\Document;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Exception;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Persistence\Repository;

class EntryRepository extends Repository
{
    const MODE_INCLUDE_UNIDENTIFIED = 1;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper
     */
    protected $dataMapper;

    public function injectDataMapper(DataMapper $dataMapper)
    {
        $this->dataMapper = $dataMapper;
    }

    public function createQueryBuilder(string $alias): QueryBuilder
    {
        $tableName = $this->dataMapper->convertClassNameToTableName($this->objectType);
        $qb = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($tableName);
        $qb->from($tableName, $alias);
        return $qb;
    }

    /**
     * @param string $type
     * @param int $mode one of the MODE_* constants
     *
     * @return array
     */
    public function getIdentifier(string $type = Document::class, int $mode = 0): array
    {
        $qb = $this->createQueryBuilder('entry');
        $qb->select('entry.identifier');
        $qb->addSelectLiteral('COUNT(entry.form_value) AS count');
        $qb->addSelectLiteral('GROUP_CONCAT(DISTINCT entry.form_label) AS form_labels');

        if ($mode & self::MODE_INCLUDE_UNIDENTIFIED) {
            $qb->getConcreteQueryBuilder()->groupBy(
                'CASE WHEN entry.identifier THEN entry.identifier ELSE entry.form_label END'
            );
        } else {
            $qb->groupBy('entry.identifier');
            $qb->andWhere('entry.identifier != ""');
        }

        $documentTable = $this->dataMapper->convertClassNameToTableName($type);
        $qb->join('entry', $documentTable, 'document', $qb->expr()->eq('document.uid', 'entry.parent_uid'));

        // an ugly implementation which gets the possible record types
        try {
            $dataMap = $this->dataMapper->getDataMap($type);
            if ($dataMap->getRecordTypeColumnName()) {
                $recordTypes = [$dataMap->getRecordType()];
                foreach ($dataMap->getSubclasses() as $subclass) {
                    $recordTypes[] = $this->dataMapper->getDataMap($subclass)->getRecordType();
                }

                $qb->andWhere($qb->expr()->in(
                    'document.' . $dataMap->getRecordTypeColumnName(),
                    $qb->createNamedParameter($recordTypes, Connection::PARAM_STR_ARRAY)
                ));
            }
        } catch (Exception $e) {
            throw new \LogicException("failed to create type list", 0, $e);
        }

        return $qb->execute()->fetchAll();
    }
}