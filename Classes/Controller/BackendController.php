<?php

namespace Hn\Formsave\Controller;

use Hn\Formsave\Exporter\Exporter;
use Hn\Formsave\Exporter\WriterInterface;
use TYPO3\CMS\Core\Error\Http\BadRequestException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class BackendController extends ActionController
{
    /**
     * @var \Hn\Formsave\Domain\Repository\DocumentRepository
     * @inject
     */
    protected $documentRepository;

    public function listAction()
    {
        $documents = $this->documentRepository->findAll();

        $this->view->assignMultiple([
            'configurations' => $GLOBALS['TYPO3_CONF_VARS']['EXT']['formsave']['writer'] ?? [],
            'documents' => $documents,
        ]);
    }

    /**
     * @param string $format
     */
    public function downloadAction(string $format = 'csv')
    {
        $writers = $GLOBALS['TYPO3_CONF_VARS']['EXT']['formsave']['writer'] ?? [];
        if (!isset($writers[$format])) {
            throw new BadRequestException("Format $format not supported");
        }

        $writerConfig = $writers[$format];
        if (!is_a($writerConfig['class'], WriterInterface::class, true)) {
            throw new \RuntimeException("Expected " . WriterInterface::class . ", got " . $writerConfig['class']);
        }

        $writer = GeneralUtility::makeInstance($writerConfig['class'], ...($writerConfig['arguments'] ?? []));
        if (!$writer instanceof WriterInterface) {
            $type = is_object($writer) ? get_class($writer) : gettype($writer);
            throw new \RuntimeException("Expected " . WriterInterface::class . ", got $type");
        }

        if (isset($writerConfig['mimeType'])) {
            header('Content-Type: ' . $writerConfig['mimeType']);
        }

        if (isset($writerConfig['filename'])) {
            header('Content-Disposition: attachment; filename="' . $writerConfig['filename'] . '"');
        } else {
            header('Content-Disposition: attachment');
        }

        $exporter = $this->objectManager->get(Exporter::class);
        $exporter->exportUsingQueryResult($this->documentRepository->findAll(), $writer);
        die();
    }
}
