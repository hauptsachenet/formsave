[![Latest Stable Version](https://poser.pugx.org/hn/formsave/v/stable)](https://packagist.org/packages/hn/formsave)
[![Total Downloads](https://poser.pugx.org/hn/formsave/downloads)](https://packagist.org/packages/hn/formsave)
[![Monthly Downloads](https://poser.pugx.org/hn/formsave/d/monthly)](https://packagist.org/packages/hn/formsave)
[![License](https://poser.pugx.org/hn/formsave/license)](https://packagist.org/packages/hn/formsave)

This extension allow to easily create domain models from form data.

## basic usage

- install using `composer require hn/formsave`
- create a form and add the Formsave finisher, select Generic as intent
- optionally add identifier to each field

## advanced usage

You can add own intents which also creates own domain objects. Lets go with an exampe of an "Application".

### Create a domain model class `Vendor\Extension\Domain\Model\Application`
```php
<?php

namespace Vendor\Extension\Domain\Model;

use Hn\Formsave\Domain\Model\Document;

class Application extends Document
{

}
```

### Implement extbase inheritence

Use the `ext_typoscript_setup.txt` for that.

```typo3_typoscript
config.tx_extbase.persistence.classes {
    Hn\Formsave\Domain\Model\Document {
        subclasses {
            Vendor\Extension\Domain\Model\Application = Vendor\Extension\Domain\Model\Application
        }
    }

    Vendor\Extension\Domain\Model\Application {
        mapping {
            recordType = Vendor\Extension\Domain\Model\Application
            tableName = tx_formsave_domain_model_document
        }
    }
}
```

### Extend the select fields in the form editor and in TCA

```php
// Configuration/TCA/Overrides/tx_formsave_domain_model_document.php

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem('tx_formsave_domain_model_document', 'type', [
    'Application',
    \Vendor\Extension\Domain\Model\Application::class,
]);
```
```yaml
# Configuration/Form/FormEngineSetup.yaml

TYPO3:
  CMS:
    Form:
      prototypes:
        standard:
          formElementsDefinition:
            Form:
              formEditor:
                propertyCollections:
                  finishers:
                    1531314970:
                      editors:
                        110:
                          selectOptions:
                            1531319170:
                              value: 'Vendor\Extension\Domain\Model\Application'
                              label: 'Application'
```

### Add Behavior to your formdata

Look into `Hn\Formsave\Domain\Finisher\FormsaveFinisher`.
There are a few signals in which you can change the behavior.
One example would be to send an email when a user applies.
