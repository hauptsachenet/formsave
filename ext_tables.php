<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

if (TYPO3_MODE === 'BE') {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'Hn.Formsave',
        'web',          // Main area
        'mod1',         // Name of the module
        '',             // Position of the module
        array(          // Allowed controller action combinations
            'Backend'   => 'list, download',
        ),
        array(          // Additional configuration
            'access'    => 'user,group',
            'icon'      => 'EXT:recordlist/Resources/Public/Icons/module-list.svg',
            'labels'    => 'LLL:EXT:formsave/Resources/Private/Language/locallang_mod.xlf',
        )
    );
}
